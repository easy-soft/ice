#!/usr/local/Cellar/python3/3.4.3_2/bin/python3
# coding=utf-8

import argparse
from http.server import BaseHTTPRequestHandler, HTTPServer

parser = argparse.ArgumentParser()
parser.add_argument("host", help="The server domain or IP", type=str)
parser.add_argument("port", help="The server port", type=int)
args = parser.parse_args()

class ErlHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        length = self.headers['content-length']
        data = self.rfile.read(int(length))
        print(data)
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

if __name__ == '__main__':
    erlServer = HTTPServer((args.host, args.port), ErlHandler)
    print("Server started.");
    try:
        erlServer.serve_forever()
    except KeyboardInterrupt:
        pass

    erlServer.server_close()
    print("\nServer stopped.")
