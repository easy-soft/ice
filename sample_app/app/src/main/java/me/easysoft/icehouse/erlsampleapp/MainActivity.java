package me.easysoft.icehouse.erlsampleapp;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.net.MalformedURLException;
import java.util.ArrayList;

import me.easysoft.icehouse.erl.Config;
import me.easysoft.icehouse.erl.Erl;
import me.easysoft.icehouse.erl.storage.LogStorageConfig;

public class MainActivity extends AppCompatActivity {
    private static final String SERVER_URL = "http://159.203.69.57:9000";

    private final Handler mHandler = new MyHandler(this);

    private ArrayAdapter<String> mAdapter;

    private EditText mEventType;
    private EditText mEventMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEventType = (EditText) findViewById(R.id.eventType);
        mEventMessage = (EditText) findViewById(R.id.eventMessage);

        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        ((ListView) findViewById(R.id.list)).setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            LogStorageConfig storageConfig = new LogStorageConfig
                .LogStorageConfigBuilder(getApplication(), SERVER_URL)
                .build();
            Config config = new Config
                .ConfigBuilder(getApplication(), new LogStorage(storageConfig, mHandler))
                .schedulerTime(2000)
                .build();
            Erl.createInstance(config);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Erl.instance().finish();
    }

    public void onClick(View v) {
        try {
            Erl.log(Integer.valueOf(mEventType.getText().toString()), mEventMessage.getText().toString());
        } catch (Exception e) {

        }
    }

    private static class MyHandler extends Handler {
        private final MainActivity mActivity;

        public MyHandler(MainActivity mainActivity) {
            mActivity = mainActivity;
        }

        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();

            if(data == null) {
                return;
            }

            for(String key : data.keySet()) {
                mActivity.mAdapter.insert(data.getString(key), 0);
            }
        }
    }
}
