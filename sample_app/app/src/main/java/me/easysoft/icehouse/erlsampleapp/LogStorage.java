package me.easysoft.icehouse.erlsampleapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.util.Queue;

import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.storage.ErlLogStorage;
import me.easysoft.icehouse.erl.storage.LogStorageConfig;

public class LogStorage extends ErlLogStorage {
    private final Handler mHandler;

    public LogStorage(LogStorageConfig config, Handler handler) {
        super(config);

        mHandler = handler;
    }

    @Override
    public void store(Queue<Log<String>> logs) {
        Bundle data = new Bundle();

        int i = 0;

        for(Log<String> log : logs) {
            try {
                data.putString("" + i, log.serialize());
                ++i;
            } catch (LogException e) {
                e.printStackTrace();
            }
        }

        Message msg = mHandler.obtainMessage();
        msg.setData(data);

        mHandler.sendMessage(msg);

        super.store(logs);
    }
}
