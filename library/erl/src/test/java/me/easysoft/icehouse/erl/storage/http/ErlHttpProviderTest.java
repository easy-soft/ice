package me.easysoft.icehouse.erl.storage.http;

import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.net.URL;

import me.easysoft.icehouse.erl.BuildConfig;
import me.easysoft.icehouse.erl.mock.LogMock;
import me.easysoft.icehouse.erl.mock.ServerMock;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, manifest = Config.NONE)
public class ErlHttpProviderTest {
    @Test
    public void send_httpTrue() {
        MockWebServer server = ServerMock.getServer200();

        URL url = server.url("/").url();

        HttpProvider http = new ErlHttpProvider(url);
        boolean actual = http.send(LogMock.getLogs());

        assertTrue(actual);
    }
}
