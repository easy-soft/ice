package me.easysoft.icehouse.erl.storage.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import me.easysoft.icehouse.erl.BuildConfig;
import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.mock.LogMock;
import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.model.LogEvent;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, manifest = Config.NONE)
public class ErlDbStorageTest {
    private Context mContext;
    private LocalStorage<String> mLocalStorage;
    private List<Log<String>> mLogs = new ArrayList<>();

    @Before
    public void setUp() {
        mContext = RuntimeEnvironment.application.getApplicationContext();
        DbManager.createInstance(mContext);

        delete();

        mLocalStorage = new ErlDbStorage(mContext);
        mLogs.clear();

        for (LogEvent log : LogMock.getLogs()) {
            mLogs.add(log);
        }
    }

    @Test
    public void add_test() {
        mLocalStorage.add(mLogs);

        Assert.assertEquals(mLogs.size(), count());

        delete();

        Assert.assertEquals(0, count());
    }

    @Test
    public void pop_test() throws LogException {
        insert(mLogs);

        List<Log<String>> savedLogs = mLocalStorage.pop(mLogs.size());

        Assert.assertEquals(mLogs.size(), savedLogs.size());

        for (int i = 0; i < savedLogs.size(); ++i) {
            LogEvent expected = LogMock.getLogs().get(i);
            LogEvent actual = (LogEvent) savedLogs.get(i);

            Assert.assertEquals(expected.getMessage(), actual.getMessage());
            Assert.assertEquals(expected.getEventType(), actual.getEventType());
        }

        delete();
        Assert.assertEquals(0, count());
    }

    @Test
    public void poll_test() throws LogException {
        insert(mLogs);

        List<Log<String>> savedLogs = mLocalStorage.poll(mLogs.size());

        Assert.assertEquals(mLogs.size(), savedLogs.size());

        for (int i = 0; i < savedLogs.size(); ++i) {
            LogEvent expected = LogMock.getLogs().get(i);
            LogEvent actual = (LogEvent) savedLogs.get(i);

            Assert.assertEquals(expected.getMessage(), actual.getMessage());
            Assert.assertEquals(expected.getEventType(), actual.getEventType());
        }

        Assert.assertEquals(0, count());
    }

    private int count() {
        try {
            SQLiteDatabase db = DbManager.instance().open();
            Cursor cur = db.rawQuery(String.format("select count(*) from %s", DbHelper.LOGS_TABLE_NAME), null);

            try {
                cur.moveToFirst();
                return cur.getInt(0);
            } finally {
                cur.close();
            }
        } finally {
            DbManager.instance().close();
        }
    }

    private void delete() {
        try {
            SQLiteDatabase db = DbManager.instance().open();
            db.delete(DbHelper.LOGS_TABLE_NAME, null, null);
        } finally {
            DbManager.instance().close();
        }
    }

    private void insert(List<Log<String>> logs) throws LogException {
        try {
            SQLiteDatabase db = DbManager.instance().open();
            ContentValues values = new ContentValues(1);

            for (Log<String> log : logs) {
                values.put(DbHelper.LOGS_COLUMN_MESSAGE, log.serialize());
                db.insert(DbHelper.LOGS_TABLE_NAME, null, values);
            }
        } finally {
            DbManager.instance().close();
        }
    }
}
