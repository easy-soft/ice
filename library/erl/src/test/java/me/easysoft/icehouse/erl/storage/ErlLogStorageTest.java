package me.easysoft.icehouse.erl.storage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import me.easysoft.icehouse.erl.mock.LogMock;
import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.model.LogEvent;
import me.easysoft.icehouse.erl.storage.http.HttpProvider;
import me.easysoft.icehouse.erl.storage.local.LocalStorage;

public class ErlLogStorageTest {
    private List<Log<String>> mLogsTable;
    private boolean send;

    private Queue<Log<String>> mLogs = new ConcurrentLinkedQueue<>();
    private LogStorageConfig.LogStorageConfigBuilder mConfigBuilder;
    private Context mContext;

    @Before
    public void setUp() {
        mLogsTable = new ArrayList<>();
        mLogs.clear();

        for (LogEvent log : LogMock.getLogs()) {
            mLogs.add(log);
        }

        mContext = Mockito.mock(Context.class);
        Mockito.when(mContext.getApplicationContext()).thenReturn(mContext);

        LocalStorage<String> localStorage = new LocalStorageMock();
        HttpProvider<String> httpProvider = new HttpProviderMock();

        mConfigBuilder = new LogStorageConfig.LogStorageConfigBuilder(mContext, localStorage, httpProvider);
    }

    @Test
    public void batteryFull_noNetwork_test() {
        Intent battery = null;
        boolean isConnected = false;
        int networkType = -1;
        boolean isRoaming = false;

        boolean onlyWifi = false;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        assertCondition(storage, mLogs.size());
    }

    @Test
    public void batteryFull_withNetwork_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_DISCHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(50);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_MOBILE;
        boolean isRoaming = false;

        boolean onlyWifi = false;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, 0);

        send = false;

        assertCondition(storage, mLogs.size());

        send = true;

        assertCondition(storage, 0);
    }

    @Test
    public void batteryFull_withNetwork_onlyWifi_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_DISCHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(50);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_MOBILE;
        boolean isRoaming = false;

        boolean onlyWifi = true;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, mLogs.size());
    }

    @Test
    public void batteryFull_withNetwork_onlyWifi_wifiType_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_DISCHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(50);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_WIFI;
        boolean isRoaming = false;

        boolean onlyWifi = true;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, 0);
    }

    @Test
    public void batteryFull_withNetwork_noRoaming_connectedInRoaming_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_DISCHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(50);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_MOBILE;
        boolean isRoaming = false;

        boolean onlyWifi = true;
        boolean useRoaming = false;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, mLogs.size());
    }

    @Test
    public void batteryLow_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_DISCHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(10);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_WIFI;
        boolean isRoaming = false;

        boolean onlyWifi = false;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, mLogs.size());
    }

    @Test
    public void batteryLow_inCharging_withNetwork_test() {
        Intent battery = Mockito.mock(Intent.class);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_CHARGING);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(10);
        Mockito.when(battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1)).thenReturn(100);

        boolean isConnected = true;
        int networkType = ConnectivityManager.TYPE_MOBILE;
        boolean isRoaming = false;

        boolean onlyWifi = false;
        boolean useRoaming = true;
        int minBatteryPct = 25;

        LogStorage<String> storage = getLogStorage(battery, isConnected, networkType, isRoaming,
                onlyWifi, useRoaming, minBatteryPct);

        send = true;

        assertCondition(storage, 0);
    }

    private void assertCondition(LogStorage<String> storage, int expected) {
        storage.store(new ConcurrentLinkedQueue<>(mLogs));
        Assert.assertEquals(expected, mLogsTable.size());
    }

    private LogStorage<String> getLogStorage(Intent intent, boolean isConnected, int type,
        boolean isRoaming, boolean onlyWifi, boolean useRoaming, int minBatteryPct) {

        mockConnectivityManager(getNetworkInfoMock(isConnected, type, isRoaming));
        Mockito.when(mContext.registerReceiver(Mockito.any(BroadcastReceiver.class),
                Mockito.any(IntentFilter.class))).thenReturn(intent);

        return new ErlLogStorage(mConfigBuilder
                .onlyWifi(onlyWifi)
                .useRoaming(useRoaming)
                .minBatteryPct(minBatteryPct)
                .build());
    }

    private NetworkInfo getNetworkInfoMock(boolean isConnected, int type, boolean isRoaming) {
        NetworkInfo networkInfo = Mockito.mock(NetworkInfo.class);

        Mockito.when(networkInfo.isConnected()).thenReturn(isConnected);
        Mockito.when(networkInfo.getType()).thenReturn(type);
        Mockito.when(networkInfo.isRoaming()).thenReturn(isRoaming);

        return networkInfo;
    }

    private void mockConnectivityManager(NetworkInfo networkInfo) {
        ConnectivityManager connectivityManager = Mockito.mock(ConnectivityManager.class);
        Mockito.when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        Mockito.when(mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(connectivityManager);
    }

    private class LocalStorageMock implements LocalStorage<String> {
        @Override
        public void add(List<Log<String>> logs) {
            mLogsTable.addAll(logs);
        }

        @Override
        public List<Log<String>> pop(int maxLogs) {
            List<Log<String>> list = new ArrayList<>(maxLogs);

            if(maxLogs > mLogsTable.size()) {
                maxLogs = mLogsTable.size();
            }

            for(int i = 0; i < maxLogs; ++i) {
                list.add(mLogsTable.get(i));
            }

            return list;
        }

        @Override
        public List<Log<String>> poll(int maxLogs) {
            List<Log<String>> list = new ArrayList<>(maxLogs);

            if(maxLogs > mLogsTable.size()) {
                maxLogs = mLogsTable.size();
            }

            for(int i = 0; i < maxLogs; ++i) {
                list.add(mLogsTable.remove(0));
            }

            return list;
        }
    }

    private class HttpProviderMock implements HttpProvider<String> {

        @Override
        public boolean send(List<Log<String>> logs) {
            return send;
        }
    }
}
