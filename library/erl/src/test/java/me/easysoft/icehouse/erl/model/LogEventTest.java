package me.easysoft.icehouse.erl.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.mock.LogMock;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LogEventTest {
    private final LogEvent mLogMock;

    public LogEventTest(LogEvent logMock) {
        mLogMock = logMock;
    }

    @Parameterized.Parameters
    public static Collection<LogEvent> logs() {
        return LogMock.getLogs();
    }

    @Test
    public void serialize_isCorrect() throws JSONException, LogException {
        Log<String> log = new LogEvent(mLogMock.getEventType(), mLogMock.getMessage(), mLogMock.getLocation(), mLogMock.getDate());

        JSONObject json = new JSONObject(log.serialize());

        assertEquals(mLogMock.getEventType(), json.getInt(LogEvent.TAG_EVENT_TYPE));
        assertEquals(LogEvent.isoFormatter.format(mLogMock.getDate()), json.getString(LogEvent.TAG_DATE));
        assertEquals(mLogMock.getMessage(), json.getString(LogEvent.TAG_MESSAGE));

        if(mLogMock.getLocation() == null) {
            if(json.has(LogEvent.TAG_LOCATION)) {
                assertNull(json.getJSONObject(LogEvent.TAG_LOCATION));
            }
        } else {
            JSONObject loc = json.getJSONObject(LogEvent.TAG_LOCATION);

            assertNotNull(loc);
            assertEquals(mLogMock.getLocation().getLatitude(), loc.getDouble(LogEvent.TAG_LATITUDE), 0.0001);
            assertEquals(mLogMock.getLocation().getLongitude(), loc.getDouble(LogEvent.TAG_LONGITUDE), 0.0001);
        }
    }
}
