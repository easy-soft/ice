package me.easysoft.icehouse.erl.mock;

import android.location.Location;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.model.LogEvent;

public final class LogMock {
    final static List<LogEvent> locations = new ArrayList<>();
    final static Random r = new Random();

    static {
        for(int i = 0; i < 100; i++) {
            int type = r.nextInt();
            String message = "Test message" + i;
            Date date = new Date();
            Location loc = r.nextBoolean() ? LocationMock.getLocations().get(i) : null;

            String ser = String.format("{\"%s\":\"%s\",\"%s\":%d,\"%s\":\"%s\"",
                    LogEvent.TAG_DATE, LogEvent.isoFormatter.format(date),
                    LogEvent.TAG_EVENT_TYPE, type, LogEvent.TAG_MESSAGE, message);

            if(loc != null) {

                ser += String.format(",\"%s\":{\"%s\":%s,\"%s\":%s", LogEvent.TAG_LOCATION,
                        LogEvent.TAG_LATITUDE, Double.toString(loc.getLatitude()), LogEvent.TAG_LONGITUDE, Double.toString(loc.getLongitude()));

                if(loc.hasAltitude()) {
                    ser += String.format(",\"%s\":%s", LogEvent.TAG_ALTITUDE, Double.toString(loc.getAltitude()));
                }

                if(loc.hasSpeed()) {
                    ser += String.format(",\"%s\":%s", LogEvent.TAG_SPEED, Double.toString(loc.getSpeed()));
                }

                ser += "}";
            }

            ser += "}";

            LogEvent log = Mockito.mock(LogEvent.class);

            Mockito.when(log.getEventType()).thenReturn(type);
            Mockito.when(log.getMessage()).thenReturn(message);
            Mockito.when(log.getDate()).thenReturn(date);
            Mockito.when(log.getLocation()).thenReturn(loc);
            try {
                Mockito.when(log.serialize()).thenReturn(ser);
            } catch (LogException e) {
                e.printStackTrace();
            }

            locations.add(log);
        }
    }

    public static List<LogEvent> getLogs()
    {
        return locations;
    }
}
