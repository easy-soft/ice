package me.easysoft.icehouse.erl.mock;

import com.squareup.okhttp.mockwebserver.Dispatcher;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import java.util.Random;

public final class ServerMock {
    final static MockWebServer server = new MockWebServer();
    final static MockWebServer server200 = new MockWebServer();
    final static int[] codes = new int[] {
        200, 202, 404, 200, 500, 301, 303, 200, 200
    };

    final static Random r = new Random();

    static {
        server.setDispatcher(new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                return new MockResponse().setResponseCode(codes[r.nextInt(codes.length)]);
            }
        });

        server200.setDispatcher(new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                return new MockResponse().setResponseCode(200);
            }
        });
    }

    public static MockWebServer getServer200() {
        return server200;
    }

    public static MockWebServer getServer() {
        return server;
    }

    private ServerMock() {}
}
