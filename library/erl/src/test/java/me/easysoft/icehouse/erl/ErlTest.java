package me.easysoft.icehouse.erl;

import android.content.Context;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.storage.LogStorage;

@RunWith(RobolectricTestRunner.class)
@org.robolectric.annotation.Config(constants = BuildConfig.class, manifest = org.robolectric.annotation.Config.NONE)
public class ErlTest {
    private Queue<Log<String>> mQueue = new ConcurrentLinkedQueue<>();
    private Config.ConfigBuilder mConfigBuilder;

    @Before
    public void setUp() {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        mConfigBuilder = new Config.ConfigBuilder(context, new LogStorageMock(), null);
    }

    @Test
    public void test() throws InterruptedException {
        Erl.createInstance(mConfigBuilder.schedulerTime(1000).build());

        int type = 0;

        Erl.log(type++, "TEST");
        Erl.log(type++, "TEST");

        Thread.sleep(1500);

        Assert.assertEquals(type, mQueue.size());

        Erl.log(type++, "TEST");
        Erl.log(type++, "TEST");

        Thread.sleep(1500);

        Assert.assertEquals(type, mQueue.size());

        Erl.log(type++, "TEST");
        Erl.log(type++, "TEST");

        Erl.instance().finish();

        Thread.sleep(3000);

        Assert.assertEquals(type, mQueue.size());
    }

    private class LogStorageMock implements LogStorage<String> {
        @Override
        public void store(Queue<Log<String>> logs) {
            mQueue.addAll(logs);
        }
    }
}
