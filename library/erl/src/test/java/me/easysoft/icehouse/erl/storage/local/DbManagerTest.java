package me.easysoft.icehouse.erl.storage.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import me.easysoft.icehouse.erl.BuildConfig;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, manifest = Config.NONE)
public class DbManagerTest {
    @Before
    public void setUp() {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        DbManager.createInstance(context);
    }

    @Test
    public void connection_db_test() {
        SQLiteDatabase db1 = DbManager.instance().open();
        SQLiteDatabase db2 = DbManager.instance().open();

        Assert.assertEquals(db1, db2);

        DbManager.instance().close();
        DbManager.instance().close();

        db2 = DbManager.instance().open();

        Assert.assertFalse(db1 == db2);

        DbManager.instance().close();
    }
}
