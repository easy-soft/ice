package me.easysoft.icehouse.erl.mock;

import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;

import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class LocationMock {
    final static List<Location> locations = new ArrayList<>();
    final static Random r = new Random();

    static {
        for(int i = 0; i < 100; i++) {
            Location loc = Mockito.mock(Location.class);
            Mockito.when(loc.getLatitude()).thenReturn(r.nextDouble());
            Mockito.when(loc.getLongitude()).thenReturn(r.nextDouble());
            Mockito.when(loc.hasAltitude()).thenReturn(r.nextBoolean());
            Mockito.when(loc.getAltitude()).thenReturn(r.nextDouble());
            Mockito.when(loc.hasSpeed()).thenReturn(r.nextBoolean());
            Mockito.when(loc.getSpeed()).thenReturn(r.nextFloat());

            locations.add(loc);
        }
    }

    public static List<Location> getLocations()
    {
        return locations;
    }

    private LocationMock() {}
}
