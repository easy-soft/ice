package me.easysoft.icehouse.erl.storage.local;

import java.util.List;

import me.easysoft.icehouse.erl.model.Log;

public interface LocalStorage<T> {
    /**
     * Add the logs
     * @param logs is the {@code java.util.List} of {@code me.easysoft.icehouse.erl.model.Log}
     */
    void add(List<Log<T>> logs);

    /**
     * Retrieves logs from head
     * @param maxLogs is the maximum number of logs that poll can return
     * @return list of logs from the head
     */
    List<Log<T>> pop(int maxLogs);

    /**
     * Retrieves and removes the logs from head
     * @param maxLogs is the maximum number of logs that poll can return
     * @return list of logs removed from the head
     */
    List<Log<T>> poll(int maxLogs);
}
