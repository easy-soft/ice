package me.easysoft.icehouse.erl.storage;

import java.util.Queue;

import me.easysoft.icehouse.erl.model.Log;

public interface LogStorage<T> {
    /**
     * The method stores the logs
     * @param logs is the {@code java.util.Queue} of {@code me.easysoft.icehouse.erl.model.Log}
     */
    void store(Queue<Log<T>> logs);
}
