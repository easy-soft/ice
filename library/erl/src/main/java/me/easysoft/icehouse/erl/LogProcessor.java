package me.easysoft.icehouse.erl;

import android.support.annotation.NonNull;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import me.easysoft.icehouse.erl.model.Log;

class LogProcessor extends ScheduledThreadPoolExecutor implements Runnable {
    private final Config mConfig;
    private ConcurrentLinkedQueue<Log<String>> mQueue;
    private ScheduledFuture<?> mSf;

    public LogProcessor(Config config) {
        super(1);

        mConfig = config;
        mQueue = new ConcurrentLinkedQueue<>();

        mSf = scheduleWithFixedDelay(this, 0, config.getSchedulerTime(), TimeUnit.MILLISECONDS);
    }

    public void addLog(@NonNull Log<String> log) {
        mQueue.add(log);
    }

    @Override
    public void run() {
        Queue<Log<String>> storageQueue = mQueue;
        mQueue = new ConcurrentLinkedQueue<>();
        mConfig.getLogStorage().store(storageQueue);
    }

    @Override
    public void shutdown() {
        mSf.cancel(false);

        if(mSf.isDone() && !mQueue.isEmpty()) {
            submit(this);
        }

        super.shutdown();
    }
}
