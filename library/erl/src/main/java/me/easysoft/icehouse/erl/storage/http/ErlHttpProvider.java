package me.easysoft.icehouse.erl.storage.http;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.model.Log;

public class ErlHttpProvider implements HttpProvider<String> {
    public static final String JSON = "application/json; charset=UTF-8";
    private static final String HTTP_METHOD = "POST";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final int CONNECTION_TIMEOUT = 1000 * 30;
    private static final int READ_TIMEOUT = 1000 * 30;

    protected final URL mUrl;
    protected final String mContentType;

    public ErlHttpProvider(@NonNull String serverUrl) throws MalformedURLException {
        this(serverUrl, JSON);
    }

    public ErlHttpProvider(@NonNull String serverUrl, @NonNull String contentType) throws MalformedURLException {
        this(new URL(serverUrl), contentType);
    }

    public ErlHttpProvider(@NonNull URL url) {
        this(url, JSON);
    }

    public ErlHttpProvider(@NonNull URL url, @NonNull String contentType) {
        mUrl = url;
        mContentType = contentType;
    }

    @Override
    public boolean send(List<Log<String>> logs) {
        if(logs == null) {
            throw new IllegalArgumentException("The instance of Log<String>... parameter is NULL.");
        }

        List<String> sLogs = new ArrayList<>(logs.size());

        for (Log<String> log : logs) {
            try {
                sLogs.add(log.serialize());
            } catch (LogException e) {
                e.printStackTrace();
            }
        }

        String data = String.format("[%s]", TextUtils.join(",", sLogs));

        try {
            HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();
            try {
                conn.setDoOutput(true);
                conn.setRequestMethod(HTTP_METHOD);
                conn.setUseCaches(false);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setRequestProperty(CONTENT_TYPE, mContentType);

                OutputStream os = conn.getOutputStream();

                try {
                    os.write(data.getBytes());
                    os.flush();
                } finally {
                    os.close();
                }

                if(conn.getResponseCode() >= HttpURLConnection.HTTP_OK) {
                    return true;
                }
            } finally {
                conn.disconnect();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }
}
