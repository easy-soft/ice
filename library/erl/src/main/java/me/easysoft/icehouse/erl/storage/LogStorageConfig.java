package me.easysoft.icehouse.erl.storage;

import android.content.Context;
import android.support.annotation.NonNull;

import java.net.MalformedURLException;
import java.net.URL;

import me.easysoft.icehouse.erl.storage.local.ErlDbStorage;
import me.easysoft.icehouse.erl.storage.local.LocalStorage;
import me.easysoft.icehouse.erl.storage.http.ErlHttpProvider;
import me.easysoft.icehouse.erl.storage.http.HttpProvider;

public class LogStorageConfig {
    private final Context mContext;
    private final LocalStorage<String> mLocalStorage;
    private final HttpProvider<String> mHttpProvider;
    private final boolean mOnlyWifi;
    private final boolean mUseRoaming;
    private final int mMinBatteryPct;
    private final int mMaxBatchLogs;

    private LogStorageConfig(LogStorageConfigBuilder builder) {
        mContext = builder.mNewContext;
        mLocalStorage = builder.mNewLocalStorage;
        mHttpProvider = builder.mNewHttpProvider;
        mOnlyWifi = builder.mNewOnlyWifi;
        mUseRoaming = builder.mNewUseRoaming;
        mMinBatteryPct = builder.mNewMinBatteryPct;
        mMaxBatchLogs = builder.mNewMaxBatchLogs;
    }

    public Context getContext() {
        return mContext;
    }

    public LocalStorage<String> getLocalStorage() {
        return mLocalStorage;
    }

    public HttpProvider<String> getHttpProvider() {
        return mHttpProvider;
    }

    public boolean isOnlyWifi() {
        return mOnlyWifi;
    }

    public boolean useRoaming() {
        return mUseRoaming;
    }

    public float getMinBatteryPct() {
        return mMinBatteryPct;
    }

    public int getMaxBatchLogs() {
        return mMaxBatchLogs;
    }

    public static class LogStorageConfigBuilder {
        private final boolean ONLY_WIFI = false;
        private final boolean USE_ROAMING = true;
        private final int MIN_BATTERY_PCT = 25;
        private final int MAX_BATCH_LOGS = 25;

        private final Context mNewContext;
        private final LocalStorage<String> mNewLocalStorage;
        private final HttpProvider<String> mNewHttpProvider;

        private boolean mNewOnlyWifi;
        private boolean mNewUseRoaming;
        private int mNewMinBatteryPct;
        private int mNewMaxBatchLogs;

        public LogStorageConfigBuilder(@NonNull Context newContext, @NonNull String serverUrl) throws MalformedURLException {
            this(newContext, new ErlDbStorage(newContext), serverUrl);
        }

        public LogStorageConfigBuilder(@NonNull Context newContext, @NonNull URL url) {
            this(newContext, new ErlDbStorage(newContext), url);
        }

        public LogStorageConfigBuilder(@NonNull Context newContext, @NonNull HttpProvider<String> newHttpProvider) {
            this(newContext, new ErlDbStorage(newContext), newHttpProvider);
        }

        public LogStorageConfigBuilder(@NonNull Context newContext, @NonNull LocalStorage<String> newLocalStorage, @NonNull String serverUrl) throws MalformedURLException {
            this(newContext, newLocalStorage, new ErlHttpProvider(serverUrl));
        }

        public LogStorageConfigBuilder(@NonNull Context newContext, @NonNull LocalStorage<String> newLocalStorage, @NonNull URL url) {
            this(newContext, newLocalStorage, new ErlHttpProvider(url));
        }

        public LogStorageConfigBuilder(Context newContext, LocalStorage<String> newLocalStorage, HttpProvider<String> newHttpProvider) {
            if(newContext == null) {
                throw new IllegalArgumentException("The instance of Context parameter is NULL.");
            }

            if(newLocalStorage == null) {
                throw new IllegalArgumentException("The instance of LocalStorage parameter is NULL.");
            }

            if(newHttpProvider == null) {
                throw new IllegalArgumentException("The instance of HttpProvider parameter is NULL.");
            }

            mNewContext = newContext;
            mNewLocalStorage = newLocalStorage;
            mNewHttpProvider = newHttpProvider;

            this
                .onlyWifi(ONLY_WIFI)
                .useRoaming(USE_ROAMING)
                .minBatteryPct(MIN_BATTERY_PCT)
                .maxBatchLogs(MAX_BATCH_LOGS);
        }

        /**
         * Define if to send or not the logs to the server only in a wifi connection is available
         * @param newOnlyWifi
         * @return instance of {@code me.easysoft.icehouse.erl.storage.LogStorageConfigBuilder}
         */
        public LogStorageConfigBuilder onlyWifi(boolean newOnlyWifi) {
            mNewOnlyWifi = newOnlyWifi;
            return this;
        }

        /**
         * Define if to send or not the logs to the server also in roaming
         * @param newUseRoaming
         * @return instance of {@code me.easysoft.icehouse.erl.storage.LogStorageConfigBuilder}
         */
        public LogStorageConfigBuilder useRoaming(boolean newUseRoaming) {
            mNewUseRoaming = newUseRoaming;
            return this;
        }

        /**
         * Define the maximum number of messages that will be send to the server per every batch
         * @param newMaxBatchLogs
         * @return instance of {@code me.easysoft.icehouse.erl.storage.LogStorageConfigBuilder}
         */
        public LogStorageConfigBuilder maxBatchLogs(int newMaxBatchLogs) {
            mNewMaxBatchLogs = newMaxBatchLogs;
            return this;
        }

        /**
         * Define the minimum percentage of battery that allow the lib to send the logs to the server
         * @param newMinBatteryPct
         * @return instance of {@code me.easysoft.icehouse.erl.storage.LogStorageConfigBuilder}
         */
        public LogStorageConfigBuilder minBatteryPct(int newMinBatteryPct) {
            mNewMinBatteryPct = newMinBatteryPct;
            return this;
        }

        public LogStorageConfig build() {
            return new LogStorageConfig(this);
        }
    }
}
