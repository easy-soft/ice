package me.easysoft.icehouse.erl;

import me.easysoft.icehouse.erl.location.ErlLocationProvider;
import me.easysoft.icehouse.erl.location.LocationProvider;
import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.model.LogEvent;

public final class Erl implements EventRemoteLogger<String> {
    private static Erl sInstance;

    private final LocationProvider mLocationProvider;
    private final LogProcessor mProcessor;

    private Erl(Config config) {
        mLocationProvider = config.getLocationProvider();
        mProcessor = new LogProcessor(config);

        if(mLocationProvider != null && mLocationProvider instanceof ErlLocationProvider) {
            ((ErlLocationProvider) mLocationProvider).registerUpdates();
        }
    }

    /**
     * Creates the instance of {@code me.easysoft.icehouse.erl.Erl}
     * @param config is the configuration for {@code me.easysoft.icehouse.erl.Erl}
     * @return the created instance of {@code me.easysoft.icehouse.erl.Erl}
     */
    public static synchronized Erl createInstance(Config config) {
        if(config == null) {
            throw new IllegalArgumentException("The instance of Config parameter is NULL.");
        }

        if(sInstance == null) {
            sInstance = new Erl(config);
        }

        return sInstance;
    }

    /**
     * The method returns the previously created instance
     * @return instance of {@code me.easysoft.icehouse.erl.Erl}
     */
    public static synchronized Erl instance() {
        if(sInstance != null) {
            return sInstance;
        }

        throw new IllegalArgumentException("Erl was not initialized. Call createInstance() first!");
    }

    /**
     * The method logs the event
     * @param eventType is the event type of type {@code int}
     * @param message is the event message of type {@code String}
     */
    public static void log(int eventType, String message) {
        instance().log(new LogEvent(eventType, message));
    }

    @Override
    public EventRemoteLogger log(Log<String> log) {
        if(log == null) {
            throw new IllegalArgumentException("The instance of Log parameter is NULL.");
        }

        if(log instanceof LogEvent && mLocationProvider != null) {
            ((LogEvent) log).setLocation(mLocationProvider.getLocation());
        }

        mProcessor.addLog(log);

        return this;
    }

    public synchronized void finish() {
        if(mLocationProvider != null && mLocationProvider instanceof ErlLocationProvider) {
            ((ErlLocationProvider) mLocationProvider).removeUpdates();
        }

        if(!mProcessor.isShutdown() && !mProcessor.isTerminating() && !mProcessor.isTerminated()) {
            mProcessor.shutdown();
        }

        sInstance = null;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.finalize();
        }
    }
}
