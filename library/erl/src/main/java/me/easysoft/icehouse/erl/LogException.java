package me.easysoft.icehouse.erl;

public class LogException extends Exception {
    public LogException() {
        super();
    }

    public LogException(String message) {
        super(message);
    }

    public LogException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public LogException(Throwable throwable) {
        super(throwable);
    }
}
