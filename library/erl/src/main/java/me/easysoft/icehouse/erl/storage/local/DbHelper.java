package me.easysoft.icehouse.erl.storage.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "erl_db.db";
    private static final int DB_VERSION = 1;

    public static final String LOGS_TABLE_NAME = "logs";
    public static final String LOGS_COLUMN_ID = "_id";
    public static final String LOGS_COLUMN_MESSAGE = "message";

    private static final String DB_CREATE = String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL);",
        LOGS_TABLE_NAME, LOGS_COLUMN_ID, LOGS_COLUMN_MESSAGE);

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
