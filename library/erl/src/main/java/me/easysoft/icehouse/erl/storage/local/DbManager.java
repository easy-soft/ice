package me.easysoft.icehouse.erl.storage.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public final class DbManager {
    private static DbManager sInstance;
    private final DbHelper mDbHelper;

    private int mCounter = 0;
    private SQLiteDatabase mDatabase;

    private DbManager(Context context) {
        mDbHelper = new DbHelper(context);
    }

    public static synchronized DbManager createInstance(Context context) {
        if(context == null) {
            throw new IllegalArgumentException("The instance of Context parameter is NULL.");
        }

        if(sInstance == null) {
            sInstance = new DbManager(context);
        }

        return sInstance;
    }

    public static synchronized DbManager instance() {
        if(sInstance != null) {
            return sInstance;
        }

        throw new IllegalArgumentException("DbManager is not initialized. Call createInstance() first!");
    }

    public synchronized SQLiteDatabase open() {
        if(mCounter == 0) {
            mDatabase = mDbHelper.getWritableDatabase();
        }

        ++mCounter;

        return mDatabase;
    }

    public synchronized void close() {
        --mCounter;

        if(mCounter == 0) {
            mDatabase.close();
            mDatabase = null;
        }

        if(mCounter < 0) {
            mCounter = 0;
        }
    }
}
