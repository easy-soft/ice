package me.easysoft.icehouse.erl.model;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import me.easysoft.icehouse.erl.LogException;

public class LogEvent implements Log<String> {
    public static final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz";
    public static final TimeZone utc = TimeZone.getTimeZone("UTC");
    public static final SimpleDateFormat isoFormatter = new SimpleDateFormat(ISO_FORMAT);

    public static final String TAG_DATE = "date";
    public static final String TAG_LOCATION = "location";
    public static final String TAG_EVENT_TYPE = "type";
    public static final String TAG_MESSAGE = "message";

    public static final String TAG_LATITUDE = "latitude";
    public static final String TAG_LONGITUDE = "longitude";
    public static final String TAG_ALTITUDE = "altitude";
    public static final String TAG_SPEED = "speed";

    protected Date mDate;
    protected Location mLocation;
    protected int mEventType;
    protected String mMessage;

    static {
        isoFormatter.setTimeZone(utc);
    }

    public LogEvent() {
        mDate = new Date();
    }

    public LogEvent(int eventType, String message) {
        this(eventType, message, null);
    }

    public LogEvent(int eventType, String message, Location location) {
        this(eventType, message, location, new Date());
    }

    public LogEvent(int eventType, String message, Location location, Date date) {
        mEventType = eventType;
        mMessage = message;
        mDate = date;
        mLocation = location;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public void setEventType(int eventType) {
        mEventType = eventType;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Date getDate() {
        return mDate;
    }

    public Location getLocation() {
        return mLocation;
    }

    public int getEventType() {
        return mEventType;
    }

    public String getMessage() {
        return mMessage;
    }

    @Override
    public String serialize() throws LogException {
        JSONObject logObject = new JSONObject();

        try {
            logObject.put(TAG_DATE, isoFormatter.format(mDate));
            logObject.put(TAG_EVENT_TYPE, mEventType);
            logObject.put(TAG_MESSAGE, mMessage);

            if (mLocation != null) {
                JSONObject locationObject = new JSONObject();

                locationObject.put(TAG_LATITUDE, mLocation.getLatitude());
                locationObject.put(TAG_LONGITUDE, mLocation.getLongitude());

                if (mLocation.hasAltitude()) {
                    locationObject.put(TAG_ALTITUDE, mLocation.getAltitude());
                }

                if (mLocation.hasSpeed()) {
                    locationObject.put(TAG_SPEED, mLocation.getSpeed());
                }

                logObject.put(TAG_LOCATION, locationObject);
            }
        } catch (JSONException e) {
            throw new LogException(e);
        }

        String data = logObject.toString();

        if(data == null) {
            throw new LogException("Invalid serialization of LogEvent.");
        }

        return data;
    }

    @Override
    public void populate(String data) throws LogException {
        try {
            JSONObject logObject = new JSONObject(data);

            mDate = isoFormatter.parse(logObject.getString(TAG_DATE));
            mEventType = logObject.getInt(TAG_EVENT_TYPE);
            mMessage = logObject.getString(TAG_MESSAGE);

            if(logObject.has(TAG_LOCATION)) {
                JSONObject locObject = logObject.getJSONObject(TAG_LOCATION);

                mLocation = new Location((String) null);
                mLocation.setLatitude(locObject.getDouble(TAG_LATITUDE));
                mLocation.setLongitude(locObject.getDouble(TAG_LONGITUDE));

                if(locObject.has(TAG_ALTITUDE)) {
                    mLocation.setAltitude(locObject.getDouble(TAG_ALTITUDE));
                }

                if(locObject.has(TAG_SPEED)) {
                    mLocation.setSpeed((float) locObject.getDouble(TAG_SPEED));
                }
            }
        } catch (Exception e) {
            throw new LogException(e);
        }
    }
}
