package me.easysoft.icehouse.erl;

import me.easysoft.icehouse.erl.model.Log;

public interface EventRemoteLogger<T> {
    /**
     * Logs the {@code Log} object
     * @param log is the {@code Log} object
     * @return self instance of {@code EventRemoteLogger}
     */
    EventRemoteLogger log(Log<T> log);
}
