package me.easysoft.icehouse.erl.storage.http;

import java.util.List;

import me.easysoft.icehouse.erl.model.Log;

public interface HttpProvider<T> {
    /**
     * The method sends the list of logs to the server
     * @param logs is the {@code java.util.List} of {@code me.easysoft.icehouse.erl.model.Log}
     * @return {@code true} if the logs were sent or
     *         {@code false} if the logs were not sent
     */
    boolean send(List<Log<T>> logs);
}
