package me.easysoft.icehouse.erl.storage.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import me.easysoft.icehouse.erl.LogException;
import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.model.LogEvent;

public class ErlDbStorage implements LocalStorage<String> {
    private final String SQL_SELECT = String.format("SELECT * FROM %s ORDER BY %s ASC LIMIT ?", DbHelper.LOGS_TABLE_NAME, DbHelper.LOGS_COLUMN_ID);
    private final String SQL_DELETE = String.format("DELETE FROM %1$s WHERE %2$s IN (SELECT %2$s FROM %1$s ORDER BY %2$s ASC LIMIT ?)", DbHelper.LOGS_TABLE_NAME, DbHelper.LOGS_COLUMN_ID);

    private final DbManager mDdbManager;

    public ErlDbStorage(@NonNull Context context) {
        mDdbManager = DbManager.createInstance(context);
    }

    @Override
    public void add(@NonNull List<Log<String>> logs) {
        try {
            SQLiteDatabase db = mDdbManager.open();
            ContentValues values = new ContentValues(1);

            for (Log<String> log : logs) {
                try {
                    values.put(DbHelper.LOGS_COLUMN_MESSAGE, log.serialize());
                    db.insert(DbHelper.LOGS_TABLE_NAME, null, values);
                } catch (LogException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            mDdbManager.close();
        }
    }

    @Override
    public List<Log<String>> pop(int maxLogs) {
        try {
            SQLiteDatabase db = mDdbManager.open();
            Cursor cursor = db.rawQuery(SQL_SELECT, new String[] { String.valueOf(maxLogs) });

            try {
                List<Log<String>> logs = new ArrayList<>();

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    try {
                        LogEvent log = new LogEvent();
                        log.populate(cursor.getString(cursor.getColumnIndex(DbHelper.LOGS_COLUMN_MESSAGE)));
                        logs.add(log);
                    } catch (LogException e) {
                        e.printStackTrace();
                        db.delete(DbHelper.LOGS_TABLE_NAME, String.format("%s = ?", DbHelper.LOGS_COLUMN_ID),
                            new String[] { String.valueOf(cursor.getInt(cursor.getColumnIndex(DbHelper.LOGS_COLUMN_ID))) });
                    }

                    cursor.moveToNext();
                }

                return logs;
            } finally {
                cursor.close();
            }
        } finally {
            mDdbManager.close();
        }
    }

    @Override
    public List<Log<String>> poll(int maxLogs) {
        List<Log<String>> logs = pop(maxLogs);

        try {
            SQLiteDatabase db = mDdbManager.open();
            db.execSQL(SQL_DELETE, new Object[] { maxLogs });
        } finally {
            mDdbManager.close();
        }

        return logs;
    }
}
