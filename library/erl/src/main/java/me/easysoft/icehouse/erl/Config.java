package me.easysoft.icehouse.erl;

import android.content.Context;

import me.easysoft.icehouse.erl.location.ErlLocationProvider;
import me.easysoft.icehouse.erl.location.LocationProvider;
import me.easysoft.icehouse.erl.storage.LogStorage;

public class Config {
    private final Context mContext;
    private final LocationProvider mLocationProvider;
    private final LogStorage<String> mLogStorage;
    private final long mSchedulerTime;


    private Config(ConfigBuilder builder) {
        mContext = builder.mNewContext;
        mLocationProvider = builder.mNewLocationProvider;
        mLogStorage = builder.mNewLogStorage;
        mSchedulerTime = builder.mNewSchedulerTime;
    }

    public Context getContext() {
        return mContext;
    }

    public LocationProvider getLocationProvider() {
        return mLocationProvider;
    }

    public LogStorage<String> getLogStorage() {
        return mLogStorage;
    }

    public long getSchedulerTime() {
        return mSchedulerTime;
    }

    public static class ConfigBuilder {
        private final long SCHEDULER_TIME = 60000;

        private final Context mNewContext;
        private LocationProvider mNewLocationProvider;
        private LogStorage<String> mNewLogStorage;
        private long mNewSchedulerTime;

        public ConfigBuilder(Context newContext, LogStorage<String> newLogStorage) {
            this(newContext, newLogStorage, new ErlLocationProvider(newContext));
        }

        public ConfigBuilder(Context newContext, LogStorage<String> newLogStorage, LocationProvider newLocationProvider) {
            if(newContext == null) {
                throw new IllegalArgumentException("The instance of Context parameter is NULL.");
            }

            if(newLogStorage == null) {
                throw new IllegalArgumentException("The instance of LogStorage parameter is NULL.");
            }

            mNewContext = newContext;
            mNewLogStorage = newLogStorage;

            this
                .locationProvider(newLocationProvider)
                .schedulerTime(SCHEDULER_TIME);
        }

        /**
         * Override the default LocationProvider the the lib normally use to find the actual location
         * @param newLocationProvider is the new {@code me.easysoft.icehouse.erl.location.LocationProvider}
         * @return instance of {@code me.easysoft.icehouse.erl.ConfigBuilder}
         */
        public ConfigBuilder locationProvider(LocationProvider newLocationProvider) {
            mNewLocationProvider = newLocationProvider;
            return this;
        }

        /**
         * Define the scheduler time in milliseconds
         * @param newSchedulerTime is the new scheduler time in milliseconds of type {@code int}
         * @return instance of {@code me.easysoft.icehouse.erl.ConfigBuilder}
         */
        public ConfigBuilder schedulerTime(long newSchedulerTime) {
            mNewSchedulerTime = newSchedulerTime;
            return this;
        }

        public Config build() {
            return new Config(this);
        }
    }
}
