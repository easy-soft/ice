package me.easysoft.icehouse.erl.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

public class ErlLocationProvider implements LocationListener, LocationProvider {
    private static final int TIME = 1000 * 60;

    protected Location mLocation = null;
    protected Context mContext;
    protected LocationManager mLocationManager;

    private boolean isRegistered = false;

    public ErlLocationProvider(Context context) {
        if(context == null) {
            throw new IllegalArgumentException("The instance of Context parameter is NULL.");
        }

        mContext = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if(hasPermission()) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = mLocationManager.getBestProvider(criteria, true);

            if (provider != null) {
                try {
                    mLocation = mLocationManager.getLastKnownLocation(provider);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public Location getLocation() {
        return mLocation;
    }

    public void registerUpdates() {
        if(hasPermission()) {
            try {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME, 100, this);
                isRegistered = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME, 100, this);
                isRegistered = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void removeUpdates() {
        if(isRegistered) {
            try {
                mLocationManager.removeUpdates(this);
                isRegistered = false;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(isBetterLocation(location, mLocation)) {
            mLocation = location;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    protected boolean isGpsEnabled() {
        try {
            return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            return false;
        }
    }

    protected boolean isNetworkEnabled() {
        try {
            return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            return false;
        }
    }

    protected boolean hasPermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return mContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }

        PackageManager pm = mContext.getPackageManager();

        return pm.checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, mContext.getPackageName()) == PackageManager.PERMISSION_GRANTED
                || pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, mContext.getPackageName()) == PackageManager.PERMISSION_GRANTED;
    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TIME;
        boolean isSignificantlyOlder = timeDelta < -TIME;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
