package me.easysoft.icehouse.erl.model;

import me.easysoft.icehouse.erl.LogException;

public interface Log<T> {
    /**
     * The method that return the serialized object
     * @return the serialized {@code me.easysoft.icehouse.erl.model.Log} object
     * @throws me.easysoft.icehouse.erl.LogException
     */
    T serialize() throws LogException;

    /**
     * The method that deserialize the serialized object
     * @param data is the serialized {@code me.easysoft.icehouse.erl.model.Log} object
     * @throws me.easysoft.icehouse.erl.LogException
     */
    void populate(T data) throws LogException;
}
