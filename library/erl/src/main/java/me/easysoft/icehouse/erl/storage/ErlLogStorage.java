package me.easysoft.icehouse.erl.storage;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import me.easysoft.icehouse.erl.model.Log;
import me.easysoft.icehouse.erl.storage.http.HttpProvider;
import me.easysoft.icehouse.erl.storage.local.LocalStorage;

public class ErlLogStorage implements LogStorage<String> {
    protected final LogStorageConfig mConfig;

    public ErlLogStorage(LogStorageConfig config) {
        if(config == null) {
            throw new IllegalArgumentException("The instance of LogStorageConfig parameter is NULL.");
        }

        mConfig = config;
    }

    @Override
    public void store(Queue<Log<String>> logs) {
        boolean useInternet = useNetwork();

        LocalStorage<String> localStorage = mConfig.getLocalStorage();
        HttpProvider<String> httpProvider = mConfig.getHttpProvider();
        int batch = mConfig.getMaxBatchLogs();

        if(useInternet) {
            List<Log<String>> localLogs;

            do {
                localLogs = localStorage.pop(batch);

                if(localLogs.size() > 0) {
                    if(!httpProvider.send(localLogs)) {
                        useInternet = false;
                        break;
                    }

                    localStorage.poll(batch);
                }
            } while (localLogs.size() >= batch);
        }

        List<Log<String>> list = new ArrayList<>(batch);

        while (!logs.isEmpty()) {
            list.add(logs.poll());

            if(list.size() >= batch || (logs.isEmpty() && !list.isEmpty())) {
                if(useInternet && !httpProvider.send(list)) {
                    useInternet = false;
                }

                if(!useInternet) {
                    localStorage.add(list);
                }

                list.clear();
            }
        }
    }

    private boolean useNetwork() {
        if(isBatteryLow()) {
            return false;
        }

        Context ctx = mConfig.getContext();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            (ctx.checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                    ctx.checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)) {
            return false;
        }

        ConnectivityManager connManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connManager == null) {
            return false;
        }

        NetworkInfo info = connManager.getActiveNetworkInfo();

        if(info == null || !info.isConnected()) {
            return false;
        }

        if(mConfig.isOnlyWifi()) {
            return info.getType() == ConnectivityManager.TYPE_WIFI;
        }

        return !(!mConfig.useRoaming() && info.isRoaming());
    }

    private boolean isBatteryLow() {
        Context ctx = mConfig.getContext();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ctx.checkSelfPermission(Manifest.permission.BATTERY_STATS) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        Intent batteryIntent = ctx.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        if(batteryIntent == null) {
            return false;
        }

        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        if(status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL) {
            return false;
        }

        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        return !(level < 0 || scale <= 0 || (level * 100 / scale) >= mConfig.getMinBatteryPct());
    }
}
