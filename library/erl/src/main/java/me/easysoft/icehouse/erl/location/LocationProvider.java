package me.easysoft.icehouse.erl.location;

import android.location.Location;

public interface LocationProvider {
    /**
     * Return the actual {@code android.location.Location} info
     * @return location object of type {@code android.location.Location}
     */
    Location getLocation();
}
