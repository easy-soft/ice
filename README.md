Configuration of Erl Logger library
==============

Manifest permissions
--------------
	android.permission.ACCESS_COARSE_LOCATION
	android.permission.ACCESS_FINE_LOCATION
	android.permission.BATTERY_STATS
	android.permission.INTERNET
	android.permission.ACCESS_NETWORK_STATE

Initialization of Erl Logger library
--------------
	// Url of the server.
	// The server will accept a list of Log objects
	// serialized in JSON format, with or without location:
	// [
	//  { "date" : "2015-09-21T09:13:10.943 UTC",
	//    "location" : {
	//       "latitude" : 0.1904001442465052,
	//       "longitude" : 0.5584840557656348
	//    },
	//    "type" : 1,
	//    "message" : "Message"
	//  },
	//  { "date" : "2015-09-21T09:13:10.943 UTC",
	//    "type" : 1,
	//    "message" : "Message"
	//  },
	//  ...
	// ]
	String url = "server url";

	// Configuration object to configure how the app will use the local
	// storage and the remote server.
	// With the default configuration the library will use an internal
	// db to keep the logs offline and it will also use an external
	// server to save the logs that will receive all of those logs in
	// JSON format.
	LogStorageConfig storageConfig = new LogStorageConfig.LogStorageConfigBuilder(getContext(), url)
		// Max number of messages that the library will send to the
		// server in a single http request.
		// Default value if not changed: 25
		.maxBatchLogs(25)

		// Boolean true/false that will allow or not the library to
		// send the logs to the http server ONLY if the application
		// is connected via WiFi.
		// Default value if not changed: false
		.onlyWifi(false)

		// Boolean true/false that will allow or not the library to
		// send the logs to the http server ALSO if the application
		// is connected in roaming.
		// Default value if not changed: true
		.useRoaming(true)

		// Minimum value, in percentage, of the battery under which
		// the library will not send via http request the logs but
		// will store it to the local db.
		// Default value if not changed: 25
		// Range of values: 0-100
		.minBatteryPct(25)

		// Create the object LogStorageConfig starting from the configuration of the builder.
		.build();

	// LogStorage<T> object that wrap both storages:
	// - the LocalStorage<T> that define how to save locally the logs
	// - the HttpProvider<T> that define how to save to the server the logs
	// those two parameters if not redefined to the config object are
	// by default:
	// - ErlDbStorage that is the LocalStorage<String> default implementation
	// - ErlHttpProvider that is the HttpProvide<String> default implementation
	LogStorage<String> logStorage = new ErlLogStorage(storageConfig);

	// Configuration object to configure how the app will schedule
	// the saving of the logs and which LocationProvider the library
	// will use to get the current location.
	Config config = new Config.ConfigBuilder(getContext(), logStorage)
		// LocationProvider to define how to get the location.
		// Default value if not changed: new ErlLocationProvider(getContext())
		.locationProvider(new ErlLocationProvider(getContext()))

		// Time in milliseconds that will set the scheduler time
		// Default value if not changed: 60000 milliseconds
		.schedulerTime(60000)

		// Create the object Config starting from the configuration of the builder.
		.build();

	// Static method that will create a singleton instance of the logger
	Erl.createInstance(config);

Using of Erl Logger library
--------------
	int eventType = 1;
	String eventMessage = "message";
	// To Log an event we will need just to call the .log(int, String) method.
	Erl.log(eventType, eventMessage);

Finishign of Erl Logger library
--------------
	Erl.instance().finish()